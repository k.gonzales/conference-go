from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests

#function to make requests to use the Pexels API 
def get_pexels_photo(city, state):
    query = f'{city} {state}'
    url = f'https://api.pexels.com/v1/search?query={query}'
    headers = {'Authorization': PEXELS_API_KEY}
    response = requests.get(url, headers = headers)
    api_dict = response.json()
    return api_dict['photos'][0]['src']['original']
    


#function to make requests to use the Open Weather ABI
def get_weather_data(city, state):
    key = OPEN_WEATHER_API_KEY
#use geocoding api to translate city and state of location to lat and lon
    coordinates_url = f'http://api.openweathermap.org/geo/1.0/direct?q={city},{state},840&appid={key}'
    coordinates_response = requests.get(coordinates_url)
    coordinates_dict = coordinates_response.json()
    lat = coordinates_dict[0]['lat']
    lon = coordinates_dict[0]['lon']
#use current weather data api to get the current weather for the location
    weather_url = f'https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={key}&units=imperial'
    weather_response = requests.get(weather_url)
    weather_dict = weather_response.json()
    temperature = {
        'temperature':weather_dict['main']['temp'],
        'description':weather_dict['weather'][0]['description']
     }
    return temperature